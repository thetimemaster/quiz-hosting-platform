module.exports = {
  bracketSpacing: false,
  jsxBracketSameLine: true,
  singleQuote: true,
  tabWidth: 4,
  useTabs: true,
  trailingComma: 'all',
};

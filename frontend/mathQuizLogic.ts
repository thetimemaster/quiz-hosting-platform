import {ActionType, MathQuizAction} from './mathQuizActions.js';
import {MathQuizAppState, QuizAppRoute, QuizState} from './mathQuizTypes.js';

export const QuizStateReducer = (
	state: QuizState,
	action: MathQuizAction,
): QuizState => {
	switch (action.type) {
		case ActionType.SelectAnswer:
			return {...state, selectedAnswer: action.newSelectedAnswer};
		case ActionType.ConfirmAnswer:
			return state.selectedAnswer === null
				? state
				: {
						...state,
						questions: state.questions.map((q, i) =>
							i === state.currentQuestion
								? {
										...q,
										// Warunek na nulla jest sprawdzany wyżej ale przewyższa myślenie kompilatora TS
										status: 'ANS',
										selected: state.selectedAnswer,
								  }
								: {...q},
						),
						selectedAnswer: null,
				  };
		case ActionType.SelectQuestion:
			return {
				...state,
				currentQuestion: action.newIndex,
				selectedAnswer: null,
			};
		case ActionType.SpentSecond:
			return {
				...state,
				questions: state.questions.map((q, i) =>
					i === state.currentQuestion
						? {
								...q,
								time: q.status !== 'NA' ? q.time : q.time + 1,
						  }
						: {...q},
				),
			};
		default:
			return state;
	}
};

export const DefaultState: MathQuizAppState = {
	appRoute: QuizAppRoute.SelectQuiz,
	loginState: {
		username: null,
		sessionId: null,
	},
	introOverlay: false,
	mobileInfoOpen: false,
	quizState: null,
	quizes: [],
};

export const MathQuizReducer = (
	state: MathQuizAppState,
	action: MathQuizAction,
): MathQuizAppState => {
	switch (action.type) {
		case ActionType.Initialize:
			return DefaultState;
		case ActionType.SelectAnswer:
			return {
				...state,
				quizState: QuizStateReducer(state.quizState, action),
			};
		case ActionType.ConfirmAnswer:
			const quizState = QuizStateReducer(state.quizState, action);
			const wasLastQuestion = quizState.questions.every(
				(q) => q.status !== 'NA',
			);
			return {
				...state,
				mobileInfoOpen: wasLastQuestion ? true : state.mobileInfoOpen,
				quizState,
			};
		case ActionType.SelectQuestion:
			return {
				...state,
				mobileInfoOpen: false,
				quizState: QuizStateReducer(state.quizState, action),
			};
		case ActionType.ChangeRoute:
			return {
				...state,
				appRoute: action.newState,
				mobileInfoOpen: false,
			};
		case ActionType.SpentSecond:
			return state.appRoute === QuizAppRoute.MainQuiz && state.quizState != null
				? {
						...state,
						quizState: QuizStateReducer(state.quizState, action),
				  }
				: state;
		case ActionType.ToggleIntro:
			return {...state, introOverlay: !state.introOverlay};
		case ActionType.ToggleMobileInfoPanel:
			return {...state, mobileInfoOpen: !state.mobileInfoOpen};
		case ActionType.SetQuizData:
			return {
				...state,
				appRoute: action.newQuiz ? QuizAppRoute.MainQuiz : state.appRoute,
				quizState: action.newQuiz,
			};
		case ActionType.LoadAvailableQuizes:
			return {
				...state,
				quizes: action.quizes,
			};
		case ActionType.SetSessionInfo:
			return {
				...state,
				loginState: {
					sessionId: action.sessionId,
					username: action.username,
				}
			}
		case ActionType.SetApplicationMessage:
			return {
				...state,
				currentMessage: action.message,
			}
		default:
			return state;
	}
};

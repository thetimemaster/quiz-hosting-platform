import {IAction, IState, Reducer} from "./node_modules/pseudo-react-redux/index";
import {
    ActionType,
    ChangeRouteAction,
    DispatchChangeRoute,
    DispatchLoadAvailableQuizes,
    DispatchSetQuiz,
    DispatchSetSessionInfo, MathQuizAction,
    SetSessionInfoAction
} from "./mathQuizActions.js";
import {AvailableQuiz, MathQuizAppState, QuizAppRoute, QuizStatus} from "./mathQuizTypes.js";
import {ApiCall, ApiMethod} from "./libApi.js";
import {FetchQuiz} from "./mathQuizStorage.js";

export const actionLoggerMiddleware = (next: Reducer<MathQuizAppState, MathQuizAction>) =>
    (state: MathQuizAppState, action: MathQuizAction): MathQuizAppState => {
    if (action.type !== ActionType.SpentSecond) {
        console.log(`ACTION: ${JSON.stringify(action)}
        STACK: ${new Error().stack}`);
    }

    return next(state, action);
}

export const updateLoginStateMiddleware = (next: Reducer<MathQuizAppState, MathQuizAction>) =>
    (state: MathQuizAppState, action: MathQuizAction): MathQuizAppState => {
    if (action.type === ActionType.SetSessionInfo) {
        const sessionInfoAction = action as SetSessionInfoAction;

        if (state.loginState.sessionId !== sessionInfoAction.sessionId) {
            document.cookie = `sessionId=${sessionInfoAction.sessionId}`;
            onNewSessionId(sessionInfoAction.sessionId);
        }
    }

    if (action.type === ActionType.ChangeRoute
        && (action as ChangeRouteAction).newState === QuizAppRoute.SelectQuiz) {
        loadAvailableQuizes();
    }

    const a = next(state, action);

    console.log(JSON.stringify(a));
    return a;
}

export const onNewSessionId = (sessionId) => {
    ApiCall({
        method: ApiMethod.GET,
        route: '/user/refreshSessionInfo',
        body: null,
        onResponse: (code, body) => {
            // Load available quizes when we have correct token
            DispatchSetSessionInfo(body.username, body.sessionId);
            loadAvailableQuizes();
        }
    });
}

const loadAvailableQuizes = () => {
    ApiCall({
        method: ApiMethod.GET,
        route: '/quiz',
        body: null,
        onResponse: (qcode, qbody) => {
            if (qcode !== 200) return;
            DispatchLoadAvailableQuizes(qbody);

            (qbody as AvailableQuiz[]).forEach(async q => {
                if (q.status === QuizStatus.STARTED) {
                    const newQuiz = await FetchQuiz(q.id);
                    DispatchSetQuiz(newQuiz);
                    DispatchChangeRoute(QuizAppRoute.MainQuiz);
                }
            })
        },
    });
}

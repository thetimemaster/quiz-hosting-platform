import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {SimpleButton} from './Buttons.js';
import {DispatchChangeRoute, DispatchSetQuiz} from '../mathQuizActions.js';
import {FetchQuiz} from '../mathQuizStorage.js';
import {QuizAppRoute, QuizStatus} from "../mathQuizTypes.js";

export const LoadQuiz = async (
	quizId: number,
) => {
	try {
		const newQuiz = await FetchQuiz(quizId)
		DispatchSetQuiz(newQuiz);
	} catch (e) {
		console.log(e);
	}
};

export const SelectQuizScreen: PseudoReactComponent = (params: {
	quizes: {
		id: number,
		title: string,
		status: QuizStatus
	}[]
}) => ({
	tag: 'section',
	id: 'select-quiz-screen',
	c: [
		{tag: 'h1', className: 'intro-message-title', c: 'Wybierz quiz:'},
		{
			tag: 'div',
			className: 'quiz-list-wrapper',
			c: [
				...params.quizes.map((q, i) =>
				SimpleButton({
					onClick: async () => {
						await LoadQuiz(q.id);
					},
					text: q.title,
					classes: `button-ultrawide button-select-quiz ${q.status === QuizStatus.FINISHED ? 'button-done' : ''}`,
				})),
				SimpleButton({
					text: 'Panel użytkownika',
					classes: 'button-ultrawide button-important',
					onClick: () => {
						DispatchChangeRoute(QuizAppRoute.Profile);
					}
				})
			]
		},
	],
});

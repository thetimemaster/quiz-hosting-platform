import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {
	ConfirmAnswerButton,
	NextQuestionButton,
	PrevQuestionButton,
	SingleAnswerButton,
	ToggleIntroductionButton,
} from './Buttons.js';
import {StatusDisplayer} from './StatusDisplayer.js';
import {Answer, QuestionState, QuizStatus} from '../mathQuizTypes.js';
import {FormatTime} from "./Timer.js";

export const QuestionScreen: PseudoReactComponent = (params: {
	title: string;
	answers: Answer[];
	status: QuestionState;
	selectedAnswer: number;
	isFirstQuestion: boolean;
	isLastQuestion: boolean;
	questionIndex: number;
	quizStatus: QuizStatus;
	correctAnswer: number | null;
	avgTime: number | undefined;
}) => ({
	tag: 'section',
	id: 'current-question',
	c: [
		ToggleIntroductionButton({}),
		{
			tag: 'div',
			className: 'question-box',
			c: {
				tag: 'h1',
				className: `question-self ${
					params.title.length > 30 ? 'long-question' : ''
				}`,
				c: params.title,
			},
		},
		{
			tag: 'div',
			className: 'answers-wrapper',
			c: params.answers.map((answer, i) =>
				SingleAnswerButton({
					text: answer.text,
					isSelected: i === params.selectedAnswer,
					index: i,
					isDisabled: params.status !== 'NA',
					isVisiblyCorrect: i === params.correctAnswer,
				}),
			),
		},
		{
			tag: 'div',
			className: 'avg-time-wrapper',
			c: params.avgTime == null
				? []
				: `Avg time: ${FormatTime(params.avgTime)}`
		},
		{
			tag: 'div',
			className: 'nav-wrapper',
			c: [
				PrevQuestionButton({
					isDisabled: params.isFirstQuestion,
					questionIndex: params.questionIndex - 1,
				}),
				params.status === 'NA'
					? ConfirmAnswerButton({
							isDisabled: params.selectedAnswer === null,
					  })
					: StatusDisplayer({status: params.status}),
				NextQuestionButton({
					isDisabled: params.isLastQuestion,
					questionIndex: params.questionIndex + 1,
				}),
			],
		},
	],
});

import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {SimpleButton} from './Buttons.js';
import {DispatchChangeRoute, DispatchSetQuiz} from "../mathQuizActions.js";
import {QuizAppRoute} from "../mathQuizTypes.js";

export const SummaryScreen: PseudoReactComponent = (params: {
	penalty: number;
	correct: number;
	all: number;
}) => ({
	tag: 'article',
	id: 'summary-screen',
	c: [
		{tag: 'h1', c: 'Zakończyłeś quiz!'},
		{
			tag: 'p',
			id: 'summary-self',
			classes: 'button-mobile-enlarge',
			c: `Zdobyłeś ${params.correct}/${params.all} z karą: ${params.penalty}`,
		},
		{
			tag: 'div',
			className: 'restart-wrapper',
			c: [
				SimpleButton({
					text: 'Zobacz odpowiedzi',
					classes: 'button-long-text',
					onClick: () => {
						DispatchChangeRoute(QuizAppRoute.MainQuiz);
					},
				}),
				SimpleButton({
					text: 'Koniec',
					classes: 'button-long-text',
					onClick: () => {
						DispatchChangeRoute(QuizAppRoute.SelectQuiz);
						DispatchSetQuiz(null);
					},
				}),
			],
		},
	],
});

import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {Question, QuizAppRoute} from '../mathQuizTypes.js';
import {
	DispatchChangeRoute,
	DispatchConfirmAnswer,
	DispatchSelectAnswer,
	DispatchSelectQuestion,
	DispatchSetQuiz,
	DispatchToggleIntro,
} from '../mathQuizActions.js';
import {ApiCall, ApiMethod} from "../libApi.js";
import {FetchQuiz} from "../mathQuizStorage.js";

export const SimpleButton: PseudoReactComponent = (params: {
	text: string;
	isDisabled: boolean;
	classes: string;
	id?: string
	onClick: () => void;
}) => ({
	tag: 'button',
	className: `button ${params.classes ? params.classes : ''}`,
	onClick: params.onClick,
	id: params.id,
	c: params.text,
	isDisabled: params.isDisabled,
});

export const SingleAnswerButton: PseudoReactComponent = (params: {
	text: string;
	index: number;
	isSelected: boolean;
	isDisabled: boolean;
	isVisiblyCorrect: boolean;
}) =>
	SimpleButton({
		text: params.text,
		isDisabled: params.isDisabled,
		onClick: () => DispatchSelectAnswer(params.index),
		classes: `answer-button answer-button-${
			params.isSelected ? 'selected' : 'non-selected'
		} ${params.isVisiblyCorrect ? 'answer-button-correct' : ''}`,
	});

export const ConfirmAnswerButton: PseudoReactComponent = (params: {
	isDisabled: boolean;
}) =>
	SimpleButton({
		text: 'Zatwierdź',
		isDisabled: params.isDisabled,
		classes: 'confirm-answer-button',
		onClick: () => DispatchConfirmAnswer(),
	});

export const PrevQuestionButton: PseudoReactComponent = (params: {
	isDisabled: boolean;
	questionIndex: number;
}) =>
	SimpleButton({
		text: 'Powrót',
		isDisabled: params.isDisabled,
		classes: 'nav-button nav-button-back',
		onClick: () => DispatchSelectQuestion(params.questionIndex),
	});

export const NextQuestionButton: PseudoReactComponent = (params: {
	isDisabled: boolean;
	questionIndex: number;
}) =>
	SimpleButton({
		text: 'Dalej',
		isDisabled: params.isDisabled,
		classes: 'nav-button nav-button-next',
		onClick: () => DispatchSelectQuestion(params.questionIndex),
	});

export const StartQuizButton: PseudoReactComponent = (params: {
	isDisabled: boolean;
	quizId: number;
}) =>
	SimpleButton({
		text: 'Start',
		isDisabled: params.isDisabled,
		classes: 'start-button',
		onClick: () => {
			ApiCall({
				method: ApiMethod.POST,
				route: `/quiz/${params.quizId}/start`,
				body: null,
				onResponse: async (responseCode, jsonBody) => {
					const newQuiz = await FetchQuiz(params.quizId)
					DispatchSetQuiz(newQuiz);
				},
			});
		},
	});

export const StopQuizButton: PseudoReactComponent = (params: {
	questionData: Question[],
	quizId: number,
}) =>
	SimpleButton({
		 text: 'Zakończ',
		 isDisabled: !params.questionData.every(q => q.selected != null),
		 classes: 'button-important stop-button',
		 onClick: async () => {
			 const sumTime = params.questionData.reduce((s, q) => s + q.time, 0);
			 ApiCall({
				 method: ApiMethod.POST,
				 route: `/quiz/${params.quizId}/end`,
				 body: params.questionData.map(q => ({
					 selectedIndex: q.selected,
					 time: (1.0 * q.time) / sumTime,
				 })),
				 onResponse: async (responseCode, jsonBody) => {
					 const newQuiz = await FetchQuiz(params.quizId)
					 DispatchSetQuiz(newQuiz);
					 DispatchChangeRoute(QuizAppRoute.QuizSummary);
				 },
			 });
		 },
	 });

export const ToggleIntroductionButton: PseudoReactComponent = ({}) =>
	SimpleButton({
		text: 'Przełącz wstęp',
		isDisabled: false,
		classes: 'toggle-intro-button button-long-text',
		onClick: () => DispatchToggleIntro(),
	});

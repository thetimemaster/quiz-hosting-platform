import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {StartQuizButton, ToggleIntroductionButton} from './Buttons.js';
import {ISavedScore} from '../mathQuizStorage.js';
import {QuizStatus} from "../mathQuizTypes.js";

export const IntroScreen: PseudoReactComponent = (params: {
	messageTitle: string;
	messageText: string;
	quizId: number;
	quizStatus: QuizStatus;
	bestScores: ISavedScore[];
}) => ({
	tag: 'section',
	id: 'intro-screen',
	c: [
		{tag: 'h1', className: 'intro-message-title', c: params.messageTitle},
		{
			tag: 'ul',
			className: 'intro-scores-wrapper',
			c: params.bestScores.map((score) => ({
				tag: 'li',
				c: `${score.whose} Wynik: ${score.correct}/${score.questions} z karą ${score.sumPenalty} w czasie ${score.time}s`,
			})),
		},
		{tag: 'p', className: 'intro-message-text', c: params.messageText},
		params.quizStatus !== QuizStatus.PENDING
			? ToggleIntroductionButton({})
			: {
					tag: 'div',
					className: 'start-wrapper',
					c: StartQuizButton({isDisabled: false, quizId: params.quizId}),
			  },
	],
});

import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {DispatchSelectQuestion} from '../mathQuizActions.js';
import {FormatTime} from './Timer.js';
import {QuestionState} from '../mathQuizTypes.js';

export const SingleQuestionInfo: PseudoReactComponent = (params: {
	status: QuestionState;
	index: number;
	timeSpent: number;
	isCurrent: boolean;
}) => ({
	tag: 'div',
	className: `single-question-info single-question-info-${params.status}`,
	c: [
		{
			tag: 'div',
			className: `small-status-icon small-status-icon-${params.status}`,
		},
		{
			tag: 'span',
			className: `single-question-info-text ${
				params.isCurrent ? 'is-current' : ''
			}`,
			c: `${params.index + 1}. Czas: ${FormatTime(params.timeSpent)}`,
		},
	],
	onClick: () => DispatchSelectQuestion(params.index),
});

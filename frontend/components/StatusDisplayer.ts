import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {QuestionState} from '../mathQuizTypes.js';

export const StatusDisplayer: PseudoReactComponent = (params: {
	status: QuestionState;
}) => ({
	tag: 'div',
	className: `status-displayer status-displayer-${params.status}`,
	c: params.status,
});

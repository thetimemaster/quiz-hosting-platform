import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';

export const FormatTime = (seconds) => {
	if (seconds >= 24 * 60 * 60) return 'A long time';
	const hours = Math.floor(seconds / 3600);
	const minutes = Math.floor(seconds / 60) % 60;
	seconds = seconds % 60;

	return `
    ${Math.floor(hours / 10)}${hours % 10}:${Math.floor(minutes / 10)}${
		minutes % 10
	}:${Math.floor(seconds / 10)}${seconds % 10}`;
};

export const Timer: PseudoReactComponent = (params: {time: number}) => ({
	tag: 'div',
	className: 'timer-main',
	c: `${FormatTime(params.time)}`,
});

import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {ApplicationMessage} from "../mathQuizTypes.js";

export const FooterContent: PseudoReactComponent = (props: {
    error: ApplicationMessage;
}) => ({
    tag: 'div',
    className: 'footer-content-wrapper',
    c: props.error == null
        ? []
        : {
            tag: 'div',
            className: `app-message-wrapper app-message-wrapper-${props.error.severity}`,
            c: props.error.self
        }
});

export const mapStateToFooterProps = (state) => ({
    error: state.currentMessage
});

import {PseudoReactComponent} from "../node_modules/pseudo-react-redux/index";
import {ApplicationMessageSeverity, LoginState, QuizAppRoute} from "../mathQuizTypes.js";
import {SimpleButton} from "./Buttons.js";
import {ApiCall, ApiMethod} from "../libApi.js";
import {
    DispatchChangeRoute,
    DispatchSetApplicationMessage,
    DispatchSetBackendMessage,
    DispatchSetSessionInfo
} from "../mathQuizActions.js";

const TryChangePassword = () => {
    const oldPasswordVal = (document.getElementById('pwd-change-old') as HTMLInputElement).value;
    const newPasswordVal = (document.getElementById('pwd-change-new') as HTMLInputElement).value;
    const newPasswordRep = (document.getElementById('pwd-change-rep') as HTMLInputElement).value;

    if (newPasswordVal === '' || oldPasswordVal === '' || newPasswordRep === '') {
        DispatchSetApplicationMessage({
            self: 'Wypełnij wszystkie pola.',
            severity: ApplicationMessageSeverity.INFO
        })
        return;
    }

    if (newPasswordVal !== newPasswordRep) {
        DispatchSetApplicationMessage({
            self: 'Hasła nie zgadzają się.',
            severity: ApplicationMessageSeverity.INFO
        })
        return;
    }

    ApiCall({
        method: ApiMethod.POST,
        route: '/user/changePassword',
        body: {
            oldPassword: oldPasswordVal,
            newPassword: newPasswordVal,
        },
        onResponse: (code, json) => {
            if (code === 200) {
                (document.getElementById('pwd-change-old') as HTMLInputElement).value = '';
                (document.getElementById('pwd-change-new') as HTMLInputElement).value = '';
                (document.getElementById('pwd-change-rep') as HTMLInputElement).value = '';

                DispatchSetSessionInfo(json.username, json.sessionId);
                DispatchSetApplicationMessage({
                    severity: ApplicationMessageSeverity.OK,
                    self: 'Pomyślna zmiana hasła.'
                });
            } else {
                DispatchSetBackendMessage(code, json);
            }
        }
    });
};

const Logout = () => {
    ApiCall({
        method: ApiMethod.POST,
        route: '/user/logout',
        body: null,
        onResponse: (code, json) => {
            if (code !== 200) return;
            DispatchSetSessionInfo(json.username, json.sessionId);
            DispatchChangeRoute(QuizAppRoute.SelectQuiz);
        },
    });
}

export const ProfileScreen: PseudoReactComponent = (props: {
    loginData: LoginState
}) => ({
    tag: 'div',
    className: 'profile-wrapper',
    c: [
        {tag: 'h2', c: `Zalogowano jako ${props.loginData.username}`},
        {tag: 'hr'},
        {tag: 'h3', c: 'Zmiana hasła'},
        {tag: 'label', c: 'Stare hasło'},
        {tag: 'input', id: 'pwd-change-old', type: 'password'},
        {tag: 'label', c: 'Nowe hasło'},
        {tag: 'input', id: 'pwd-change-new', type: 'password'},
        {tag: 'label', c: 'Powtórz nowe hasło'},
        {tag: 'input', id: 'pwd-change-rep', type: 'password'},
        SimpleButton({
            text: 'Zmień',
            classes: 'button-changepwd',
            onClick: TryChangePassword
         }),
        {tag: 'hr', },
        SimpleButton({
            text: 'Wyloguj',
            classes: 'button-logout',
            onClick: Logout,
        }),
        SimpleButton({
            text: 'Powrót',
            classes: 'button-back',
            onClick: () => {
                DispatchChangeRoute(QuizAppRoute.SelectQuiz);
            },
        }),
    ]
});

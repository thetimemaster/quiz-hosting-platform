import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {ApplicationMessageSeverity, LoginState} from "../mathQuizTypes.js";
import {SimpleButton} from "./Buttons.js";
import {ApiCall, ApiMethod} from "../libApi.js";
import {DispatchSetApplicationMessage, DispatchSetBackendMessage, DispatchSetSessionInfo} from "../mathQuizActions.js";


export const TryLogin = () => {
    const username = (document.getElementById('input-username') as HTMLInputElement).value;
    const password = (document.getElementById('input-password') as HTMLInputElement).value;

    if (username == null || password == null) {
        DispatchSetApplicationMessage({
            self: 'Fill username and password fields.',
            severity: ApplicationMessageSeverity.INFO
        });
        return;
    }

    ApiCall({
        method: ApiMethod.POST,
        route: '/user/login',
        body: {
            name: username,
            password,
        },
        onResponse: (code, json) => {
            if (code === 200) {
                DispatchSetSessionInfo(json.username, json.sessionId);
                DispatchSetApplicationMessage(null);
            } else {
                DispatchSetBackendMessage(code, json);
            }
        },
    });
}

export const LoginPanel: PseudoReactComponent = (props: {
    userData: LoginState;
}) => ({
    tag: 'div',
    className: 'header-login-wrapper',
    c: [
        {tag: 'h2', c: 'Zaloguj się'},
        {tag: 'hr'},
        {tag: 'label', c: 'Login'},
        {tag: 'input', id: 'input-username'},
        {tag: 'label', c: 'Hasło'},
        {tag: 'input', id: 'input-password', type: 'password'},
        SimpleButton({
            text: 'Zaloguj',
            classes: 'button-login',
            onClick: TryLogin
        }),
    ]
});


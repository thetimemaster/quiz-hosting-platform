import {PseudoReactComponent, PseudoReactRenderedComponent,} from '../node_modules/pseudo-react-redux/index';
import {IntroScreen} from './IntroScreen.js';
import {QuizSideInfo} from './QuizSideInfo.js';
import {AvailableQuiz, LoginState, MathQuizAppState, Question, QuizAppRoute, QuizStatus} from '../mathQuizTypes.js';
import {QuestionScreen} from './QuestionScreen.js';
import {SummaryScreen} from './SummaryScreen.js';
import {SelectQuizScreen} from './SelectQuizScreen.js';
import {ISavedScore} from '../mathQuizStorage.js';
import {LoginPanel} from "./LoginPanel.js";
import {ProfileScreen} from "./ProfieScreen.js";

export const QuizApp: PseudoReactComponent = (params: {
	quizId: number;
	currentQuestion: number;
	questions: Question[];
	selectedAnswer: number;
	introOverlay: boolean;
	quizRoute: QuizAppRoute;
	quizIntroTitle: string;
	quizIntroText: string;
	mobileInfoOpen: boolean;
	bestScores: ISavedScore[];
	availableQuizes: AvailableQuiz[];
	loginState: LoginState;
	quizStatus: QuizStatus;
}): PseudoReactRenderedComponent => {
	if (params.loginState.username == null) {
		return LoginPanel({
		    userData: params.loginState
	  	});
	}

	if (params.quizRoute === QuizAppRoute.SelectQuiz) {
		return SelectQuizScreen({quizes: params.availableQuizes});
	}

	if (params.quizRoute === QuizAppRoute.Profile) {
		return ProfileScreen({loginData: params.loginState});
	}

	const allQuestionsAnswered = params.questions.every(
		(q) => q.selected != null,
	);

	return {
		tag: 'article',
		id: 'quiz-screen',
		c: [
			params.quizRoute === QuizAppRoute.QuizSummary
				? SummaryScreen({
					correct: params.questions.filter(
						(q) => q.selected === q.correct,
					).length,
					all: params.questions.length,
					penalty: params.questions.reduce(
						(acc, q) =>
							acc +
							q.time +
							(q.selected === q.correct ? 0 : q.waPenalty),
						0,
					),
				})
				: params.quizStatus === QuizStatus.PENDING || params.introOverlay
				? IntroScreen({
					messageTitle: params.quizIntroTitle,
					messageText: params.quizIntroText,
					introOverlay: params.introOverlay,
					bestScores: params.bestScores,
					quizId: params.quizId,
					quizStatus: params.quizStatus,
				})
				: QuestionScreen({
						title:
							params.questions[params.currentQuestion].question,
						answers:
							params.questions[params.currentQuestion].answers,
						selectedAnswer:
							params.questions[params.currentQuestion].status ===
							'NA'
								? params.selectedAnswer
								: params.questions[params.currentQuestion]
										.selected,
						status: params.questions[params.currentQuestion].status,
						questionIndex: params.currentQuestion,
						isLastQuestion:
							params.currentQuestion ===
							params.questions.length - 1,
						isFirstQuestion: params.currentQuestion === 0,
						quizStatus: params.quizStatus,
						correctAnswer: params.questions[params.currentQuestion].correct,
						avgTime: params.questions[params.currentQuestion].avgTime,
				  }),
			QuizSideInfo({
				questions: params.questions,
				currentQuestion: params.currentQuestion,
				isMobileOpen: params.mobileInfoOpen,
				quizId: params.quizId,
				quizStatus: params.quizStatus,
				allQuestionsAnswered,
			}),
		],
	};
};

export const mapStateToQuizProps = (state: MathQuizAppState) => ({
	quizRoute: state.appRoute,
	quizStatus: state.quizState ? state.quizState.quizStatus : null,
	questions: state.quizState ? state.quizState.questions : null,
	selectedAnswer: state.quizState ? state.quizState.selectedAnswer : null,
	currentQuestion: state.quizState ? state.quizState.currentQuestion : null,
	quizIntroTitle: state.quizState ? state.quizState.title : null,
	quizIntroText: state.quizState ? state.quizState.description : null,
	introOverlay: state.introOverlay,
	mobileInfoOpen: state.mobileInfoOpen,
	bestScores: state.quizState ?
		(state.quizState.bestScores ? state.quizState.bestScores : []): [],
	availableQuizes: state.quizes,
	quizId: state.quizState ? state.quizState.quizId : null,
	loginState: state.loginState,
});

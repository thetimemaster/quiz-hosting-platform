import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {DispatchChangeRoute, DispatchSetSessionInfo, DispatchToggleMobileInfoPanel} from '../mathQuizActions.js';
import {LoginState, QuizAppRoute} from "../mathQuizTypes.js";
import {SimpleButton} from "./Buttons.js";
import {ApiCall, ApiMethod} from "../libApi.js";

export const HeaderContent: PseudoReactComponent = (props: {
	title: string;
	userData: LoginState;
}) => ({
	tag: 'div',
	className: 'header-content-wrapper',
	c: [
		{tag: 'h1', id: 'main-title', c: props.title},
		{
			tag: 'button',
			className: 'button button-burger mobile-only',
			onClick: DispatchToggleMobileInfoPanel,
		}
	],
});

export const mapStateToHeaderProps = (state) => ({
	title: state.quizState ? state.quizState.title : 'Matematyczne quizowanie!',
	userData: state.loginState,
});

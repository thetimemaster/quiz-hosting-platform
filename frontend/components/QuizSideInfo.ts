import {PseudoReactComponent} from '../node_modules/pseudo-react-redux/index';
import {Timer} from './Timer.js';
import {SingleQuestionInfo} from './SingleQuestionInfo.js';
import {Question, QuizAppRoute, QuizStatus} from '../mathQuizTypes.js';
import {SimpleButton, StopQuizButton} from './Buttons.js';
import {DispatchChangeRoute, DispatchSetQuiz} from "../mathQuizActions.js";

export const QuizSideInfo: PseudoReactComponent = (params: {
	questions: Question[];
	currentQuestion: number;
	isMobileOpen: boolean;
	allQuestionsAnswered: boolean;
	quizId: number;
	quizStatus: QuizStatus,
}) => {
	return {
		tag: 'section',
		id: 'quiz-info',
		className: params.isMobileOpen ? 'mobile-open' : 'mobile-closed',
		c: [
			{
				tag: 'div',
				id: 'questions-list',
				c: [
					Timer({
						time: params.questions.reduce(
							(acc, val) => acc + val.time,
							0,
						),
					}),
					...params.questions.map((q, i) =>
						SingleQuestionInfo({
							status: q.status,
							index: i,
							timeSpent: q.time,
							isCurrent: i === params.currentQuestion,
						}),
					),
					params.quizStatus !== QuizStatus.STARTED
						? SimpleButton({
							text: 'Powrót',
							onClick: () => {
								DispatchChangeRoute(QuizAppRoute.SelectQuiz);
								DispatchSetQuiz(null);
							},
					    })
						: StopQuizButton({
							questionData: params.questions,
							quizId: params.quizId,
						}),
				],
			},
		],
	};
};

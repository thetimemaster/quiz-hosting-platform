import {UseState} from "./node_modules/pseudo-react-redux/index";
import {MathQuizAppState} from "./mathQuizTypes.js";

export enum ApiMethod {
    GET= 'GET',
    POST = 'POST',
    PUT = 'PUT',
    PATCH = 'PATCH',
    DELETE = 'DELETE',
}

export type ApiCallParams = {
    method: ApiMethod,
    route: string,
    body: any,
    onResponse: (responseCode: number, jsonBody: any) => void
};

const apiRoot = '/api';

export const ApiCall = async (params: ApiCallParams): Promise<void> => {
    console.log(
        `${params.method}:${params.route} BODY: ${
            params.body == null ? undefined : JSON.stringify(params.body)
        }`,
    );

    const sessionId = (UseState() as MathQuizAppState).loginState.sessionId;

    try {
        const resp =
            await fetch(`${apiRoot}${params.route}`, {
                method: params.method,
                headers: {
                    'Content-Type': 'application/json',
                    'Cookie': `sessionId=${sessionId}`
                },
                body: params.body === null ? undefined : JSON.stringify(params.body),
            });

        const json = resp.status === 204 ? null : await resp.json();
        const statusCode = resp.status;

        console.log(
            `FETCH result for ${params.method} ${params.route}:
            code: ${statusCode} response: ${JSON.stringify(json)}`,
        );
        try {
            params.onResponse(statusCode, json);
        } catch (err) {
            console.log('Error while responding');
        }
    } catch (err) {
        console.log(`FETCH ERR ${params.route}: ${err}`);
        params.onResponse(500, {
           error: err,
        });
    }
};

export const standardApiOnErr = (code, json) => {
    console.log(`ERR(${code}): ${JSON.stringify(json)}`);
};

import {ISavedScore} from './mathQuizStorage.js';

export enum QuizAppRoute {
	SelectQuiz = 'SelectQuiz',
	MainQuiz = 'MainQuiz',
	ChangePassword = 'ChangePassword',
	QuizSummary = 'QuizSummary',
	Profile = 'Profile',
}

export enum QuizStatus {
	PENDING = "Pending",
	STARTED = "Started",
	FINISHED = "Finished"
}

export type Answer = {
	text: string;
};

export type QuestionState = 'NA' | 'ANS' | 'OK' | 'WA';

export type Question = {
	status: QuestionState;
	question: string;
	answers: Answer[];
	selected: number | null;
	time: number;
	waPenalty: number;
	correct: number | null;
	avgTime: number | null;
};

export type QuizState = {
	title: string;
	description: string;
	selectedAnswer: number | null;
	currentQuestion: number;
	questions: Question[];
	quizStatus: QuizStatus;
	quizId: number;
	bestScores: ISavedScore[];
	myScore: ISavedScore | null;
};

export type AvailableQuiz = {
	id: number,
	title: string,
	status: QuizStatus,
};

export enum ApplicationMessageSeverity {
	INFO = 'info',
	WARNING = 'warning',
	ERROR = 'error',
	OK = 'ok',
}

export type ApplicationMessage = {
	self: string,
	severity: ApplicationMessageSeverity
}

export type LoginState = {
	username: string,
	sessionId: string
}

export type MathQuizAppState = {
	appRoute: QuizAppRoute;
	loginState: LoginState;
	introOverlay: boolean;
	mobileInfoOpen: boolean;
	quizState: QuizState | null;
	quizes: AvailableQuiz[],
	currentMessage?: ApplicationMessage;
};

import {Dispatch, IAction} from './node_modules/pseudo-react-redux/pseudo-redux.js';
import {
	ApplicationMessage,
	ApplicationMessageSeverity,
	AvailableQuiz,
	QuizAppRoute,
	QuizState
} from './mathQuizTypes.js';
import {ISavedScore} from "./mathQuizStorage";

export enum ActionType {
	Initialize = 'Initialize',
	ChangeRoute = 'ChangeRoute',
	SelectAnswer = 'SelectAnswer',
	ConfirmAnswer = 'ConfirmAnswer',
	SelectQuestion = 'SelectQuestion',
	SpentSecond = 'SpentSecond',
	ToggleIntro = 'ToggleIntro',
	ToggleMobileInfoPanel = 'ToggleMobileInfoPanel',
	SetQuizData = 'SetQuizData',
	LoadBestScores = 'LoadBestScores',
	LoadAvailableQuizes = 'LoadAvailableQuizes',
	SetSessionInfo = 'SetSessionInfo',
	SetApplicationMessage = 'SetApplicationMessage',
}

export interface SetApplicationMessageAction {
	type: ActionType.SetApplicationMessage,
	message?: ApplicationMessage,
}

export interface SetSessionInfoAction extends IAction {
	type: ActionType.SetSessionInfo,
	username: string | null,
	sessionId: string,
}

export interface LoadAvailableQuizesAction extends IAction{
	type: ActionType.LoadAvailableQuizes,
	quizes: AvailableQuiz[],
}

export interface InitializeAction extends IAction {
	type: ActionType.Initialize;
}

export interface SelectAnswerAction extends IAction {
	type: ActionType.SelectAnswer;
	newSelectedAnswer: number;
}

export interface ConfirmAnswerAction extends IAction {
	type: ActionType.ConfirmAnswer;
}

export interface SelectQuestionAction extends IAction {
	type: ActionType.SelectQuestion;
	newIndex: number;
}

export interface ChangeRouteAction extends IAction {
	type: ActionType.ChangeRoute;
	newState: QuizAppRoute;
}

export interface SpentSecondAction extends IAction {
	type: ActionType.SpentSecond;
}

export interface ToggleIntroAction extends IAction {
	type: ActionType.ToggleIntro;
}

export interface ToggleMobileInfoPanelAction extends IAction {
	type: ActionType.ToggleMobileInfoPanel;
}

export interface LoadQuizAction extends IAction {
	type: ActionType.SetQuizData;
	newQuiz: QuizState;
}

export const DispatchSelectAnswer = (index) =>
	Dispatch({
		type: ActionType.SelectAnswer,
		newSelectedAnswer: index,
	} as IAction);

export const DispatchConfirmAnswer = () =>
	Dispatch({
		type: ActionType.ConfirmAnswer,
	});

export const DispatchSelectQuestion = (index) =>
	Dispatch({
		type: ActionType.SelectQuestion,
		newIndex: index,
	} as IAction);

export const DispatchSpentSecond = () =>
	Dispatch({
		type: ActionType.SpentSecond,
	});

export const DispatchChangeRoute = (route) =>
	Dispatch({
		type: ActionType.ChangeRoute,
		newState: route,
	} as IAction);

export const DispatchToggleIntro = () =>
	Dispatch({
		type: ActionType.ToggleIntro,
	});

export const DispatchToggleMobileInfoPanel = () =>
	Dispatch({
		type: ActionType.ToggleMobileInfoPanel,
	});

export const DispatchSetQuiz = (quizState: QuizState) =>
	Dispatch({
		type: ActionType.SetQuizData,
		newQuiz: quizState,
	} as IAction);

export const DispatchLoadAvailableQuizes = (quizes: AvailableQuiz[]) =>
	Dispatch({
		type: ActionType.LoadAvailableQuizes,
		quizes,
	 } as IAction);

export const DispatchSetSessionInfo = (username: string | null, sessionId: string) =>
	Dispatch({
		type: ActionType.SetSessionInfo,
		username,
		sessionId
	 } as IAction);

export const DispatchSetApplicationMessage = (message: ApplicationMessage | null) => {
	Dispatch({
		type: ActionType.SetApplicationMessage,
		message
	 } as IAction);
}

export const DispatchSetBackendMessage = (code: number, json:  any) => {
	Dispatch({
		 type: ActionType.SetApplicationMessage,
		 message: {
			 self: json.message,
			 severity: code / 100 === 5
			 	? ApplicationMessageSeverity.ERROR
			 	: ApplicationMessageSeverity.WARNING,
		 },
	} as IAction);
}

export type MathQuizAction =
	| InitializeAction
	| SelectAnswerAction
	| ConfirmAnswerAction
	| SelectQuestionAction
	| ChangeRouteAction
	| SpentSecondAction
	| ToggleIntroAction
	| ToggleMobileInfoPanelAction
	| LoadQuizAction
	| LoadAvailableQuizesAction
	| SetSessionInfoAction
	| SetApplicationMessageAction ;

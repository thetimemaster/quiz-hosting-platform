import {QuizState, QuizStatus} from './mathQuizTypes.js';
import {ApiCall, ApiMethod} from "./libApi.js";

export interface ISavedScore {
	whose: string,
	correct: number,
	questions: number,
	time: number,
	sumPenalty: number,
}

export interface ISavedQuestion {
	question: string;
	answers: string[];
	waPenalty: number;
}

export interface ISavedQuiz {
	title: string;
	description: string;
	questions: ISavedQuestion[];
	quizId: number;
	quizStatus: QuizStatus;
	modelSolution?: ISavedSolution,
	mySolution?: ISavedSolution,
	myScore?: ISavedScore,
	bestScores?: ISavedScore[],
}

export interface ISavedSolution {
	selectedIndex: number,
	time: number,
}

function IsSavedScore(json: any): json is ISavedScore {
	return (
		typeof json.time === 'number' &&
		typeof json.correct === 'number' &&
		typeof json.sumPenalty === 'number' &&
		typeof json.questions === 'number' &&
		typeof json.whose === 'string'
	);
}

function IsSavedQuestion(json: any): json is ISavedQuestion {
	return (
		typeof json.question === 'string' &&
		typeof json.waPenalty === 'number' &&
		Array.isArray(json.answers) &&
		json.answers.every((a) => typeof a === 'string')
	);
}

function IsSavedQuiz(json: any): json is ISavedQuiz {
	return (
		typeof json.quizStatus === 'string' &&
		typeof json.title === 'string' &&
		typeof json.quizId === 'number' &&
		typeof json.description === 'string' &&
		Array.isArray(json.questions) &&
		json.questions.every((q) => IsSavedQuestion(q))
	);
}

export const FetchQuiz = async (quizId: number): Promise<QuizState> => new Promise<QuizState>(((resolve, reject) => {
	ApiCall({
		method: ApiMethod.GET,
		route: `/quiz/${quizId}`,
		body: null,
		onResponse: (code, json) => {
			if (code !== 200) {
				reject(`Fetch returned ${code}`);
				return;
			}

			if (IsSavedQuiz(json)) {
				const savedQuiz = json as ISavedQuiz;

				resolve({
					bestScores: json.bestScores,
					myScore: json.myScore,
					quizStatus: json.quizStatus,
					quizId: json.quizId,
					title: savedQuiz.title,
					description: savedQuiz.description,
					selectedAnswer: null,
					currentQuestion: 0,
					questions: savedQuiz.questions.map((q, qi) => ({
						status: (savedQuiz.mySolution && savedQuiz.modelSolution)
							? (savedQuiz.mySolution[qi].selectedIndex === savedQuiz.modelSolution[qi].selectedIndex ? 'OK' : 'WA')
							: 'NA',
						question: q.question,
						waPenalty: q.waPenalty,
						time: savedQuiz.mySolution ? savedQuiz.mySolution[qi].time : 0,
						selected: savedQuiz.mySolution ? savedQuiz.mySolution[qi].selectedIndex : null,
						answers: q.answers.map(a => ({text: a})),
						correct: savedQuiz.modelSolution ? savedQuiz.modelSolution[qi].selectedIndex : null,
						avgTime: savedQuiz.modelSolution ? savedQuiz.modelSolution[qi].time : null,
					})),
				});
			} else {
				reject('to ISavedQuiz error');
				return;
			}
		}
	});
}));


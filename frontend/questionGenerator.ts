import {Question} from './mathQuizTypes.js';

const GetRandomInt = (max: number): number =>
	Math.floor(Math.random() * Math.floor(max));

enum Operator {
	ADD = '+',
	SUB = '-',
	MUL = '*',
	DIV = '/',
}
const operatorCount = 4;
const RandomOperator = (): Operator =>
	[Operator.ADD, Operator.SUB, Operator.MUL, Operator.DIV][
		GetRandomInt(operatorCount)
	];

const GetLimit = (k: number): number => Math.floor(Math.pow(10, k));

const WrongifyNumber = (num, wrongFactor) =>
	Math.random() < wrongFactor ? num + GetRandomInt(3) - 1 : num;

const WrongifyOperator = (operator: Operator, wrongFactor: number) =>
	Math.random() < wrongFactor ? RandomOperator() : operator;

type Formula = number | Operation;

interface Operation {
	leftFormula: Formula;
	operator: Operator;
	rightFormula: Formula;
}

const GenAnswer = (formula: Formula, wrongFactor: number): number => {
	if (typeof formula === 'object') {
		const leftValue = WrongifyNumber(
			GenAnswer(formula.leftFormula, wrongFactor),
			wrongFactor,
		);
		const operator = WrongifyOperator(formula.operator, wrongFactor);
		const rightValue = WrongifyNumber(
			GenAnswer(formula.rightFormula, wrongFactor),
			wrongFactor,
		);

		switch (operator) {
			case Operator.SUB:
				return leftValue - rightValue;
			case Operator.ADD:
				return leftValue + rightValue;
			case Operator.MUL:
				return leftValue * rightValue;
			case Operator.DIV:
				// Questions are generated in a way that produces integers, but wrongify can mess that up
				return Math.floor(leftValue / rightValue);
		}
	} else {
		return formula;
	}
};

const stringify = (formula: Formula): string => {
	if (typeof formula === 'object') {
		const leftStr = stringify(formula.leftFormula);
		const operator = formula.operator.toString();
		const rightStr = stringify(formula.rightFormula);

		return `(${leftStr} ${operator} ${rightStr})`;
	} else {
		return formula.toString();
	}
};

const genFormula = (budget: number, numberSize: number): Formula => {
	if (budget === 1) {
		return GetRandomInt(GetLimit(numberSize));
	} else {
		const leftBudget = GetRandomInt(budget - 1) + 1;
		const operator = RandomOperator();
		const rightBudget = budget - leftBudget;

		let leftFormula = genFormula(leftBudget, numberSize);
		let rightFormula = genFormula(rightBudget, numberSize);

		if (operator === Operator.DIV) {
			while (GenAnswer(rightFormula, 0) === 0)
				rightFormula = genFormula(rightBudget, numberSize);

			const modulo =
				GenAnswer(leftFormula, 0) % GenAnswer(rightFormula, 0);

			if (modulo < 0)
				leftFormula = {
					leftFormula,
					operator: Operator.ADD,
					rightFormula: -modulo,
				};

			if (modulo > 0)
				leftFormula = {
					leftFormula,
					operator: Operator.SUB,
					rightFormula: modulo,
				};
		}

		return {leftFormula, operator, rightFormula};
	}
};

export const genQuiz = (
	questionCount: number,
	hardness: number,
	numberSize: number,
	answerCount: number,
): Question[] =>
	Array.apply(null, new Array(questionCount)).map((ign, i) => {
		const formula = genFormula(hardness, numberSize);

		const correct = GenAnswer(formula, 0);
		const ansSet = new Set();
		ansSet.add(correct);

		let guard = 0;

		while (answerCount > ansSet.size && guard < 100) {
			ansSet.add(GenAnswer(formula, 0.2));
			guard++;
		}

		return {
			status: 'NA',
			question: stringify(formula),
			answers: [...ansSet]
				.map((v) => ({text: v.toString(), isOk: v === correct}))
				.sort(() => Math.random() - 0.5),
			selected: null,
			time: 0,
			waPenalty: 120,
		};
	});

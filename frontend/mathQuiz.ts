import {Subscribe, InitializeApp} from './node_modules/pseudo-react-redux/index';
import {DispatchSpentSecond} from './mathQuizActions.js';
import {MathQuizReducer} from './mathQuizLogic.js';
import {mapStateToQuizProps, QuizApp} from './components/QuizApp';
import {HeaderContent, mapStateToHeaderProps,} from './components/HeaderContent.js';
import {FooterContent, mapStateToFooterProps} from "./components/FooterContent.js";
import {actionLoggerMiddleware, onNewSessionId, updateLoginStateMiddleware} from "./mathQuizMiddleware.js";

// Subscribing main app component
Subscribe('quiz-root', mapStateToQuizProps, QuizApp);

// Subscribing header title separately - don't want to change HTML structure now
Subscribe('header-root', mapStateToHeaderProps, HeaderContent);

// Subscribing footer separately - don't want to change HTML structure now
Subscribe('footer-root', mapStateToFooterProps, FooterContent);

// Starting the app
InitializeApp(actionLoggerMiddleware(updateLoginStateMiddleware(MathQuizReducer)));

// Dispatch this every second - found no cleaner way to implement it
setInterval(() => {
	DispatchSpentSecond();
}, 1000);

function getCookie(cname) {
	const name = cname + "=";
	const decodedCookie = decodeURIComponent(document.cookie);
	const ca = decodedCookie.split(';');
	// tslint:disable-next-line:prefer-for-of
	for (let i = 0; i <ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) === ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) === 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

// Load login status
onNewSessionId(getCookie('sessionId'));

// Warn user when reloading the app
window.addEventListener('beforeunload', (e) => {
	const confirmationMessage = `Reloading the page will reset state of the app.
		 Are you sure you want to do this?`;

	(e || window.event).returnValue = confirmationMessage; // Gecko + IE
	return confirmationMessage; // Gecko + Webkit, Safari, Chrome etc.
});

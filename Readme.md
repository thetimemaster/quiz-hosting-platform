### Summary 
This app allows hosting multiple choice tests. 
Every user can solve each test once, 
and they can then see their answers score and stats, 
together with highscores of other users.

This was the final project of Web Applications course at the
University of Warsaw, this application is also the primary reason
why Pseudo-React-Redux was created (we were not allowed to use
external libraries on the frontend part.

The backend part of the application is an Node JS Express
based solution with an SQLite database. Quizes are for now
hardcored as .json assets in the app, as there is no editor
for them created yet.

### Instalation
This is a single git repository that hosts both frontend and backend
parts of the app, which are separate projects. You have to run:

`npm install`

in both backend and frontend directories and compile all the files with
TSC.

### Scripts
Following scripts can be used in the `backend` directory

* `npm run createdb` creates the database for the application and populates
it with quizzes from `data/quizes` directory. If `database.db` file existed
it is overwritten. 
* `npm start` / `npm run start` starts the local server on a default 3000 port
* `npm run test` backs up the database, if it existed and runs the server
in the background and finally runs the available tests. 
After running tests, database file is restored (if it existed).

There are currently no scripts for the frontend part, as it just contains
plain files.

### Frontend separation
You can host frontend on a different server than the backend.
Here are the changes to be made:
* remove using `indexRouter` from backend server in `App.ts`,
this will disable plain serving files from sibling `frontend` directory
* in `libApi.ts` change `apiRoot` to appropriate value
* create another server to plain serve the files in the `frontend` directory

The test script from backend will not work, if the frontend is separated.

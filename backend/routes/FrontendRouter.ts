import * as express from "express";
import {SessionRequest} from "../middleware/types";
import * as path from "path";

const protectedRouter = (csrfProtection, parseForm) => {
    const router = express.Router();

    router.get('/*', async (req: SessionRequest, res, next) => {
        let _path = req.path;

        if (_path === '/')
            _path = 'index.html';

        console.log(`xd ${_path} '${path.extname(_path)}'`);

        if (path.extname(_path) === '')
            _path = _path + '.js'

        res.sendFile(_path, {
            root: path.join(__dirname, '../../frontend'),
        });
    });

    return router;
};

export default protectedRouter;

import * as express from "express";
import demandLoggedInMiddleware from "../middleware/DemandLoggedIn";
import {SessionRequest} from "../middleware/types";
import {isQuizSolutionDTO} from "../dtos/QuizSolutionDTO";
import * as Database from '../database/quizHandler';
import {QuizStatus} from '../database/quizHandler';
import {dbInsert} from "../database/dbUtils";
import {Handler} from "express";
import {databaseConn} from "../database/databaseHandler";

const protectedRouter = (csrfProtection, parseForm) => {
    const router = express.Router();
    const quizByIdRouter = express.Router();

    type QuizByIdRequest = SessionRequest & {quizId: number};

    const checkValidIdMiddleware: Handler = (req: QuizByIdRequest, res, next) => {
        try {
            const quizId = parseInt(req.params.quizId, 10);
            req.quizId = quizId;
            next();
        } catch (err) {
            res.status(400);
            res.json({
                         message: "Quiz id must be a number."
             });
            return;
        }
    }

    router.use('/:quizId', checkValidIdMiddleware, quizByIdRouter);

    // GET quiz list
    // - 200: ListedQuizDTO[]
    router.get('/', demandLoggedInMiddleware, async (req: SessionRequest, res, next) => {
        try {
            const quizes = await Database.GetQuizList(databaseConn, req.sessionInfo.loggedUser.id);
            res.json(quizes);
        } catch (err) {
            res.status(500);
            res.json(err);
        }

    });

    // GET quiz data
    // - 200: QuizDataDTO

    quizByIdRouter.get('/', demandLoggedInMiddleware, async (req: QuizByIdRequest, res, next) => {
        const currentQuizState =
            await Database.GetMyQuizStatus(databaseConn, req.sessionInfo.loggedUser.id, req.quizId);

        try {
            const data = await Database.GetQuizData(
                databaseConn,
                req.sessionInfo.loggedUser.id,
                req.quizId,
                currentQuizState !== QuizStatus.PENDING,
                currentQuizState === QuizStatus.FINISHED
            );
            res.json(data);
        } catch (err) {
            res.status(err.code);
            res.json(err.data);
        }
    });

    // POST to start quiz session can only run if didnt try before and no other session is active
    // - body: none
    // - 204: Empty
    quizByIdRouter.post('/start', demandLoggedInMiddleware, async (req: QuizByIdRequest, res, next) => {
        const currentQuizState =
            await Database.GetMyQuizStatus(databaseConn, req.sessionInfo.loggedUser.id, req.quizId);

        if (currentQuizState !== QuizStatus.PENDING) {
            res.status(401);
            res.json({message: 'Quiz has already started'});
            return;
        }

        try {
            await dbInsert(databaseConn, 'UserQuiz', {
                userId: req.sessionInfo.loggedUser.id,
                quizId: req.quizId,
                status: QuizStatus.STARTED,
                startTime: Date.now()
            });
        } catch (err) {
            console.log(err);
        }

        res.status(204);
        res.send();
    });

    // POST my answers format and end session
    // - body: QuizSolutionDTO
    // - 204: Empty
    quizByIdRouter.post('/end', demandLoggedInMiddleware, async (req: QuizByIdRequest, res, next) => {
        if (!isQuizSolutionDTO(req.body)) {
            res.status(400);
            res.send({message: 'Invalid QuizSolutionDTO.'});
            return;
        }

        await Database.InsertUserQuizSolution(databaseConn, req.sessionInfo.loggedUser.id, req.quizId, req.body);

        res.status(204);
        res.send();
    });

    return router;
};

export default protectedRouter;

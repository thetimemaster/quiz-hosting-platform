import * as Database from "../database/authHandler";
import * as express from "express";

import demandLoggedInMiddleware from "../middleware/DemandLoggedIn";
import {SessionRequest} from "../middleware/types";
import SessionInfoDTO from "../dtos/SessionInfoDTO";
import {databaseConn} from "../database/databaseHandler";

const protectedRouter = (csrfProtection, parseForm) => {
    const router = express.Router();

    router.get('/refreshSessionInfo', async (req: SessionRequest, res, next) => {
        console.log(JSON.stringify(req.sessionInfo));
        const response: SessionInfoDTO = {
            sessionId: req.sessionInfo.newSessionId,
            username: req.sessionInfo.loggedUser ?
                req.sessionInfo.loggedUser.name:
                null,
        };

        res.json(response);
    });

    router.post('/login', async (req: SessionRequest, res, next) => {
        if (req.sessionInfo.loggedUser) {
            res.status(403);
            res.json({message: "You cannot log in while logged in"});
            return;
        }

        try {

            const sessionId = await Database.TryLogin(databaseConn, req.body.name, req.body.password);
            await Database.RemoveSession(databaseConn, req.sessionInfo.newSessionId);
            res.status(200);
            res.json({sessionId, username: req.body.name})
        } catch (error) {
            res.status(error.code);
            res.json(error.data);
        }
    });

    router.post('/logout', demandLoggedInMiddleware, async (req: SessionRequest, res, next) => {
        await Database.RemoveSession(databaseConn, req.sessionInfo.newSessionId);
        const loggedOutSessionId = await Database.NewSession(databaseConn, null);
        res.status(200);
        res.json({sessionId: loggedOutSessionId, username: null})
    });

    router.post('/changePassword', demandLoggedInMiddleware, parseForm, async (req: SessionRequest, res, next) => {
        try {
            const newSessionId = await Database.TryChangePassword(
                databaseConn, req.sessionInfo.loggedUser.id, req.body.oldPassword, req.body.newPassword);

            res.status(200);
            res.json({sessionId: newSessionId, username: req.sessionInfo.loggedUser.name})
        } catch (error) {
            res.status(error.code);
            res.json(error.data);
        }
    });

    return router;
};

export default protectedRouter;

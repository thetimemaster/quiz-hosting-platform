export class MultipleEventAwaiter {
    private readonly onResolve: () => any;
    private readonly onReject: (err: any) => any;
    private pending: number;
    private hasFinished: boolean = false;

    public NewEvent = () => this.NewEvents(1);

    public NewEvents = eventCount => {
        if (this.hasFinished) return;
        this.pending += eventCount;
    };

    public EventFinished = () => {
        if (this.hasFinished) return;

        this.pending--;
        if (this.pending === 0) {
            this.onResolve();
            this.hasFinished = true;
        }
    }

    public EventRejected = err => {
        if (this.hasFinished) return;

        this.onReject(err);
        this.hasFinished = true;
    }

    constructor(onResolve, onReject, initialTasks = 1) {
        this.onResolve = onResolve;
        this.onReject = onReject;
        this.pending = initialTasks;
    }
}

const express = require("express");
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require("morgan");
const csrf = require("csurf");
const bodyParser = require("body-parser");

import userRouter from "./routes/UserRouter";
import quizRouter from "./routes/QuizRouter";
import indexRouter from "./routes/FrontendRouter";

import {CloseDatabase} from "./database/databaseHandler";

import catchErrorMiddleware from "./middleware/CatchError";
import catch404Middleware from "./middleware/Catch404";
import handleSessionsMiddleware from "./middleware/HandleSessions";

const app = express();

const csrfProtection = csrf({ cookie: true });
const parseForm = bodyParser.urlencoded({ extended: false })

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// routers
const apiRouter = express.Router();
// check for session cookie end set new if absent/old
apiRouter.use(handleSessionsMiddleware);
apiRouter.use('/user/', userRouter(csrfProtection, parseForm));
apiRouter.use('/quiz/', quizRouter(csrfProtection, parseForm));

app.use('/api/', apiRouter);
app.use('/', indexRouter(csrfProtection, parseForm));

// catch 404 and forward to error handler
app.use(catch404Middleware);

// error handler
app.use(catchErrorMiddleware);

module.exports = app;

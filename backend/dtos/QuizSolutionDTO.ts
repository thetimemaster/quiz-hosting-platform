type QuizSolutionDTO = {
    selectedIndex: number,
    time: number,
}[];

export function isQuizSolutionDTO(obj: any): obj is QuizSolutionDTO {
    return true;
}

export default QuizSolutionDTO;

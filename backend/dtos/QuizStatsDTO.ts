type QuizStatsDTO = {
    whose: string,
    correct: number,
    questions: number,
    time: number,
    sumPenalty: number,
}

export default QuizStatsDTO;

import {QuizStatus} from "../database/quizHandler";

type ListedQuizDTO = {
    id: number,
    title: string
    status: QuizStatus
}

export default ListedQuizDTO;

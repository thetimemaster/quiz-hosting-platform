import QuizStatsDTO from "./QuizStatsDTO";
import QuestionDTO from "./QuestionDTO";
import QuizSolutionDTO from "./QuizSolutionDTO";
import {QuizStatus} from "../database/quizHandler";

type QuizDataDTO = {
    quizId: number,
    title: string,
    description: string,

    quizStatus: QuizStatus,

    questions?: QuestionDTO[],      // only present if started or finished

    myScore?: QuizStatsDTO,         // only present if finished
    bestScores?: QuizStatsDTO[],      // only present if finished
    modelSolution?: QuizSolutionDTO // only present if finished
    mySolution?: QuizSolutionDTO    // only present if finished
}

export default QuizDataDTO;

import AnswerDTO from "./AnswerDTO";

type QuestionDTO = {
    waPenalty: number,
    question: string,
    answers: AnswerDTO[],
    correctAnswer?: number,
}

export default QuestionDTO;

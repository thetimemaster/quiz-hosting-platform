import {SessionId} from "../entities/Session";

type SessionInfoDTO = {
    sessionId: SessionId,
    username?: string
}

export default SessionInfoDTO;

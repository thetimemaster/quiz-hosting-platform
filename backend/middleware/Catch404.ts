const createError = require("http-errors");
import {Handler} from "express";

const catch404Middleware: Handler = (req, res, next) => {
    next(createError(404));
}

export default catch404Middleware;

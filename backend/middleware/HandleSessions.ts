import {GetSessionInfo} from "../database/authHandler";
import {databaseConn} from "../database/databaseHandler";
import {SessionHandler} from "./types";

const handleSessionsMiddleware: SessionHandler = async (req, res, next) => {
    const currentSessionId = req.cookies.sessionId;
    req.sessionInfo = await GetSessionInfo(databaseConn, currentSessionId);
    next();
}

export default handleSessionsMiddleware;

import {SessionHandler} from "./types";

const demandLoggedInMiddleware: SessionHandler = (req, res, next) => {
    console.log('DEMAND LOGGED IN');
    if (!req.sessionInfo.loggedUser) {
        res.status(401);
        res.json({
            message: 'You need to be logged in to use this feature.'
         });
        return;
    }
    next();
};

export default demandLoggedInMiddleware;

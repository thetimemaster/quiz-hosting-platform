import {NextFunction, Response, Request} from "express";
import {SessionInfo} from "../entities/Session";

export type SessionRequest = Request & {sessionInfo: SessionInfo};
export type SessionHandler = (req: SessionRequest, res: Response, next: NextFunction) => void;


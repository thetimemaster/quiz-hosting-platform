import {User} from "./User";

export type SessionId = string;

export type SessionInfo = {
    newSessionId: SessionId,
    loggedUser?: User,
};

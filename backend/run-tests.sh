#!/bin/bash

DBFILE=database.db

[ -f "$DBFILE" ]
db=$?

if [ ${db} -eq 0 ]; then
  echo "Old database found, moving."
  mv ${DBFILE} ${DBFILE}.cp
fi

node database/createDatabase.js > /dev/null
node bin/www.js > /dev/null &
npx mocha -r ts-node/register tests/all.ts

pkill -P $$

rm -f DBFILE

if [ ${db} -eq 0 ]; then
  echo "Restoring old database."
  mv ${DBFILE}.cp ${DBFILE}
else
  rm ${DBFILE}
fi

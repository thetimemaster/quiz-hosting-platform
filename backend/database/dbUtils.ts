import {Database} from "sqlite3";
import * as sha256 from "js-sha256";

export function HashPassword(password: string): string {
    return sha256.sha256('verycoolsalt' + password);
}


type DbInsertResult = {
    res: any,
    rowId: number,
}

export function dbInsert(db: Database, tableName: string, data: {} ): Promise<DbInsertResult> {
    return new Promise(((resolve, reject) => {
        db.run(
            `INSERT INTO ${tableName}(${Object.keys(data).join(', ')})
                 VALUES (${Object.keys(data).map(o => '?').join(', ')});`,
            Object.values(data),
            function (err, res) {
                if (err) {
                    reject({
                        code: 500,
                        message: err
                    });
                } else {
                    resolve({
                        res,
                        rowId: this.lastID
                    });
                }
            }
        );
    }))
}

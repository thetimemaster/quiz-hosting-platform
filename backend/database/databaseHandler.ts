import * as sqlite3 from 'sqlite3';
import {Database} from 'sqlite3';

export const DB_NAME = 'database.db';

export function maybeThrowInternalError(err: any, reject: (reason?: any) => void): boolean {
    if (err) {
        reject({
            code: 500,
            data: {
                message: "Unknow SQL error.",
                stack: new Error().stack
            }
        });
        return err != null;
    }
}

export function OpenDatabase() {
    const databaseConn: Database = new sqlite3.Database(DB_NAME);

    databaseConn.on('trace', sql => {
        console.log('CALL: ' + sql);
    });

    databaseConn.on('error', err => {
        console.log('ERR: ' + JSON.stringify(err));
    })

    return databaseConn;
}

export function CloseDatabase(db: Database): void {
    db.close();
}

export const databaseConn = OpenDatabase();

// exit handler
process.on('exit', () => {
    CloseDatabase(databaseConn);
});

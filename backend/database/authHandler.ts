import {Database} from "sqlite3";
import {SessionId, SessionInfo} from "../entities/Session";
import {maybeThrowInternalError} from "./databaseHandler";
import {HashPassword} from "./dbUtils";

export function NewSessionId(): SessionId {
    const chars = '0123456789abcdef';
    const uuid = ("xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx").replace(/[x]/g, (c) => {
        // tslint:disable-next-line:no-bitwise
        const r = (Math.random() * 16) % 16 | 0;
        return chars[r];
    });
    return uuid;
}

export async function NewSession(db: Database, userId: number | null): Promise<SessionId> {
    return new Promise<SessionId>((resolve, reject) => {
        const sessionId = NewSessionId();

        db.run(`INSERT INTO Session(token, userId, expires) VALUES (?, ?, ?)`, [
            sessionId,
            userId,
            Date.now() + 30 * 60 * 1000 // add 30 minutes
        ]);

        resolve(sessionId);
    });
}


export async function TryLogin(db: Database, userName: string, password: string): Promise<SessionId> {
    return new Promise<SessionId>((resolve, reject) => {
        const pwdhash = HashPassword(password);

        db.get(
            `SELECT * FROM User WHERE User.name = ? AND User.pwdhash = ?`,
            [userName, pwdhash],
            async (err, row) => {
                if (maybeThrowInternalError(err, reject)) return;
                if (row == null) {
                    reject({code: 403, data: {message: "Invalid username or password."}});
                    return ;
                }

                const loggedSession = await NewSession(db, row.id);
                resolve(loggedSession);
            }
        );
    });
}

export async function RemoveSession(db: Database, sessionId: SessionId): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        db.run(`DELETE FROM Session WHERE Session.token = ?`, [sessionId], async (err, res) => {
            if (maybeThrowInternalError(err, reject)) return;
            resolve();
        });
    });

}

export async function GetSessionInfo(db: Database, sessionId: SessionId): Promise<SessionInfo> {
    return new Promise<SessionInfo>(((resolve, reject) => {
        db.get(
            `SELECT * FROM Session LEFT JOIN User ON Session.userId = User.id WHERE Session.token = ?;`,
            [sessionId],
            async (err, row) => {
                // not a valid session
                if (err || row == null || row.expires < Date.now()) {
                    if (!err && row != null) {
                        await RemoveSession(db, sessionId);
                    }
                    const newSession = await NewSession(db, null);
                    resolve({newSessionId: newSession, loggedUser: null});
                    return;
                }

                resolve({
                    newSessionId: sessionId,
                    loggedUser: row.name ? {
                        name: row.name,
                        id: row.userId,
                    } : null,
                });
            }
        );
    }));
}

export async function TryChangePassword(db: Database, userId: number,
                                        oldPassword: string, newPassword: string): Promise<SessionId> {
    return new Promise<SessionId>(((resolve, reject) => {
        console.log(`Password change from ${oldPassword} to ${newPassword}`)
        db.get(
            `SELECT * FROM User WHERE id = ? AND pwdhash = ?;`,
            [userId, HashPassword(oldPassword)],
            async (err, row) => {
                if (maybeThrowInternalError(err, reject)) return;

                // wrong password
                if (row == null) {
                    reject({code: 403, data: {message: "Wrong old password."}});
                    return;
                }

                db.run(
                    `UPDATE User SET pwdhash = ? WHERE id = ?;`,
                    [HashPassword(newPassword), userId],
                    (err, res) => {
                        if (maybeThrowInternalError(err, reject)) return;

                        db.run(
                            `DELETE FROM Session WHERE userId = ?;`,
                            [userId],
                            async (err, res) => {
                                if (maybeThrowInternalError(err, reject)) return;

                                try {
                                    const newSession = await NewSession(db, userId);
                                    resolve(newSession);
                                } catch (err) {
                                    reject(err);
                                }
                            }
                        );
                    }
                )
            }
        );
    }));
}

import {Database} from "sqlite3";
import QuizDataDTO from "../dtos/QuizDataDTO";
import ListedQuizDTO from "../dtos/ListedQuizDTO";
import {dbInsert} from "./dbUtils";
import {maybeThrowInternalError} from "./databaseHandler";
import QuizSolutionDTO from "../dtos/QuizSolutionDTO";
import {MultipleEventAwaiter} from "../utility/multipleEventAwaiter";
import QuestionDTO from "../dtos/QuestionDTO";

export enum QuizStatus {
    PENDING = "Pending",
    STARTED = "Started",
    FINISHED = "Finished"
}

// For initialization
export async function AddQuiz(db: Database, quiz: QuizDataDTO): Promise<void> {
    return new Promise<void>(async (resolve, reject) => {
        try {
            const quizId = await dbInsert(db, 'Quiz', {
                title: quiz.title,
                description: quiz.description
            });

            let i = 0;
            for (const q of quiz.questions) {
                try {
                    await AddQuestion(db, q, quizId.rowId, i);
                } catch(err) {
                    reject(err);
                }
                i++;
            }
            resolve();
        } catch(err) {
            reject(err);
        }
    });
}

async function AddQuestion(db: Database, question: QuestionDTO, quizId: number, myIndex: number): Promise<void> {
    return new Promise<void>(async (resolve, reject) => {
        try {
            const questionId = await dbInsert(db, 'QuizQuestion', {
                indx: myIndex,
                quizId,
                question: question.question,
                waPenalty: question.waPenalty,
                correctAnswer: question.correctAnswer
            });

            let i = 0;
            for (const a of question.answers) {
                try {
                    await AddAnswer(db, a, quizId, myIndex, i);
                } catch(err) {
                    reject(err);
                }
                i++;
            }
            resolve();
        } catch(err) {
            reject(err);
        }
    });
}

async function AddAnswer(db: Database, answer: string, quizId: number, questionIndex, myIndex: number): Promise<void> {
    return new Promise<void>(async (resolve, reject) => {
        try {
            await dbInsert(db, 'QuestionAnswer', {
                indx: myIndex,
                quizId,
                questionIndex,
                text: answer,
            });
            resolve();
        } catch(err) {
            reject(err);
        }
    });
}

// For actual work
export async function GetMyQuizStatus(db: Database, userId: number, quizId: number): Promise<QuizStatus> {
    return new Promise<QuizStatus>((resolve, reject) => {
        db.get(
            `SELECT status FROM UserQuiz WHERE userId = ? AND quizId = ?;`,
            [userId, quizId],
            (err, res) => {
                if (maybeThrowInternalError(err, reject)) return;

                if (res == null) {
                    resolve(QuizStatus.PENDING);
                    return;
                }
                resolve(res.status);
            }
        );
    });
}

export async function GetQuizData(db: Database, userId: number, quizId: number,
                                  includeQuestions: boolean, includeInsights: boolean): Promise<QuizDataDTO> {
    return new Promise<QuizDataDTO>((resolve, reject) => {
        db.get(
            `SELECT * FROM Quiz WHERE id = ?;`,
            [quizId],
            async (err, res) => {
                if (maybeThrowInternalError(err, reject)) return;
                const status = await GetMyQuizStatus(db, userId, quizId);

                const output: QuizDataDTO = {
                    quizId,
                    title: res.title,
                    description: res.description,
                    questions: [],
                    quizStatus: status,
                }

                const awaiter = new MultipleEventAwaiter(
                    () => {
                        resolve(output)
                    },
                    err => {
                        reject(err);
                    },
                    1 + (includeQuestions ? 1 : 0) + (includeInsights ? 1 : 0)
                );

                if (includeQuestions) {
                    db.all(
                        `SELECT * FROM QuizQuestion WHERE quizId = ? ORDER BY indx ASC;`,
                        [quizId],
                        (err, qres) => {
                            if (maybeThrowInternalError(err, reject)) return;

                            output.questions = qres.map(r => ({
                                question: r.question,
                                waPenalty: r.waPenalty,
                                answers: null,
                            }));

                            awaiter.NewEvents(output.questions.length);
                            awaiter.EventFinished();

                            qres.forEach((q, index) => {
                                db.all(
                                    `SELECT * FROM QuestionAnswer WHERE quizId = ? AND questionIndex = ? ORDER BY indx ASC;`,
                                    [quizId, q.indx],
                                    (err, ares) => {
                                        if (maybeThrowInternalError(err, reject)) return;
                                        output.questions[index].answers = ares.map(a => a.text);
                                        awaiter.EventFinished();
                                    }
                                )
                            });
                        }
                    )
                }

                if (includeInsights) {
                    awaiter.NewEvents(3);
                    awaiter.EventFinished();

                    // Adding model solution + average time
                    db.all(
                        `
                        SELECT
                            MAX(Sel.correct) AS correctAnswer,
                            AVG((UserQuiz.endTime - UserQuiz.startTime) / 1000 * UserQuestion.percentTimeSpent) avgTime
                        FROM
                            (
                                SELECT
                                    QuizQuestion.correctAnswer AS correct,
                                    QuizQuestion.quizId AS quizId,
                                    QuizQuestion.indx AS indx
                                FROM QuizQuestion
                                WHERE quizId = ?
                            ) AS Sel
                        LEFT JOIN UserQuestion
                                ON UserQuestion.quizId = Sel.quizId
                                AND UserQuestion.questionIndex = Sel.indx
                        LEFT JOIN
                            UserQuiz
                                ON UserQuiz.quizId = Sel.quizId
                                AND UserQuestion.userId = UserQuiz.userId
                        GROUP BY UserQuestion.questionIndex
                        ORDER BY MAX(Sel.indx) ASC;`,
                        [quizId],
                        (err, res) => {
                            if (maybeThrowInternalError(err, reject)) return;

                            output.modelSolution = res.map(row => ({
                               selectedIndex: row.correctAnswer,
                               time: Math.round(row.avgTime),
                            }));
                            awaiter.EventFinished();
                        }
                    );

                    // Adding my solution + my time
                    db.all(
                        `SELECT * FROM
                                UserQuestion
                             JOIN
                                (SELECT (endTime - startTime) / 1000 AS time FROM UserQuiz WHERE quizId = ? AND userId = ?) quiz
                             WHERE quizId = ? AND userId = ?
                             ORDER BY questionIndex ASC;`,
                        [quizId, userId, quizId, userId],
                        (err, res) => {
                            if (maybeThrowInternalError(err, reject)) return;

                            output.mySolution = res.map(row => ({
                                selectedIndex: row.selected,
                                time: Math.round(row.percentTimeSpent * row.time)
                            }));
                            awaiter.EventFinished();
                        }
                    );

                    // Adding best scores
                    db.all(
                        `
                            SELECT
                                MAX(User.name) AS whose,
                                SUM(UserQuestion.selected = QuizQuestion.correctAnswer) AS correct,
                                COUNT() AS questions,
                                MAX(UserQuiz.endTime - UserQuiz.startTime) / 1000 AS time,
                                MAX(UserQuiz.endTime - UserQuiz.startTime) / 1000 + SUM((1 - (UserQuestion.selected = QuizQuestion.correctAnswer)) * QuizQuestion.waPenalty) AS sumPenalty
                            FROM
                                UserQuiz
                            LEFT JOIN
                                UserQuestion
                                    ON UserQuestion.userId = UserQuiz.userId
                                    AND UserQuestion.quizId = UserQuiz.quizId
                            LEFT JOIN
                                QuizQuestion
                                    ON UserQuestion.quizId = QuizQuestion.quizId
                                    AND UserQuestion.questionIndex = QuizQuestion.indx
                            LEFT JOIN
                                User
                                    ON UserQuiz.userId = User.id
                                    WHERE UserQuiz.quizId = ?
                            GROUP BY UserQuestion.userId
                            ORDER BY correct DESC, sumPenalty ASC, time ASC
                            LIMIT 3;`,
                        [quizId],
                        (err, res) => {
                            if (maybeThrowInternalError(err, reject)) return;

                            output.bestScores = res;
                            awaiter.EventFinished();
                        }
                    )
                }

                awaiter.EventFinished();
            }
        )
    });
}

export async function GetQuizList(db: Database, userId: number): Promise<ListedQuizDTO[]> {
    return new Promise<ListedQuizDTO[]>((resolve, reject) => {
        db.all(
            `SELECT Quiz.id AS id, Quiz.title AS title, IFNULL(q.status, ?) AS status
             FROM Quiz LEFT JOIN (SELECT quizId, userId, status FROM UserQuiz WHERE userId = ?) q
             ON Quiz.id = q.quizId ORDER BY Quiz.id ASC;`,
            [QuizStatus.PENDING, userId],
            (err, rows) => {
                if (maybeThrowInternalError(err, reject)) return;
                resolve(rows);
            }
        )
    });
}

export async function InsertUserQuizSolution(db: Database, userId: number,
                                             quizId: number, solution: QuizSolutionDTO): Promise<void> {
    return new Promise<void>(async (resolve, reject) => {
        const status = await GetMyQuizStatus(db, userId, quizId);
        console.log('gor status');
        if (status !== QuizStatus.STARTED) {
            reject({
                code: 401,
                message: 'You cannot end this quiz now.'
            });
            return;
        }

        db.all(
            `SELECT * FROM QuizQuestion WHERE quizId = ? ORDER BY indx ASC;`,
            [quizId],
            (err, res) => {
                console.log(`CALLBACK ${err} ${res}`);
                if (maybeThrowInternalError(err, reject)) return;
                if (res.length !== solution.length) {
                    reject({
                        code: 400,
                        message: 'Wrong answer[] length.'
                    });
                    return;
                }

                db.run(`UPDATE UserQuiz SET endTime = ?, status = ? WHERE userId = ? AND quizId = ?;`,
                    [Date.now(), QuizStatus.FINISHED, userId, quizId]);

                res.forEach((q, i) => {
                    db.run(`INSERT INTO UserQuestion VALUES (?, ?, ?, ?, ?);`,
                        [i, quizId, userId, solution[i].selectedIndex, solution[i].time])
                });

                resolve();
            }
        )
    });
}

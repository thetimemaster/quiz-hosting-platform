import {Database} from "sqlite3";
import {AddQuiz} from "./quizHandler";
import * as fs from 'fs';
import {HashPassword} from "./dbUtils";
import {MultipleEventAwaiter} from "../utility/multipleEventAwaiter";
import QuizDataDTO from "../dtos/QuizDataDTO";
import {databaseConn} from "./databaseHandler";

const InitSQLCallback = (awaiter: MultipleEventAwaiter) => (err, res) => {
    if (err) {
        console.log(
            `INITIALIZATION ERR: \n ${JSON.stringify(err)}\n CONTEXT: ${this}`);

        awaiter.EventRejected(err);
    } else {
        awaiter.EventFinished();
    }
}

export async function CreateDatabase(db: Database): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        const awaiter = new MultipleEventAwaiter(
            () => resolve(),
            err => reject(err),
            7
        );

        db.serialize(() => {
            db.run(`
              CREATE TABLE User(
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              name VARCHAR(32)UNIQUE NOT NULL,
              pwdhash VARCHAR(256)NOT NULL
              );
            `, [], InitSQLCallback(awaiter));

            db.run(`
              CREATE TABLE Session(
              token VARCHAR(256)PRIMARY KEY,
              userId NUMBER REFERENCES user,
              expires NUMBER NOT NULL,
              pages NUMBER NOT NULL DEFAULT 0
              );
            `, [], InitSQLCallback(awaiter));

            db.run(`
              CREATE TABLE Quiz(
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              title TEXT UNIQE NOT NULL,
              description TEXT
              );
            `, [], InitSQLCallback(awaiter));

            db.run(`
              CREATE TABLE QuizQuestion(
              indx INTEGER NOT NULL,
              quizId NUMBER REFERENCES Quiz,
              question TEXT NOT NULL,
              waPenalty NUMBER NOT NULL,
              correctAnswer NUMBER NOT NULL,
              PRIMARY KEY(indx, quizId)
              );
            `, [], InitSQLCallback(awaiter));

            db.run(`
              CREATE TABLE QuestionAnswer(
              indx INTEGER NOT NULL,
              quizId INTEGER REFERENCES Quiz,
              questionIndex INTEGER NOT NULL,
              text TEXT NOT NULL,
              PRIMARY KEY(indx, quizId, questionIndex)
              );
            `, [], InitSQLCallback(awaiter));

            db.run(`
              CREATE TABLE UserQuiz(
              quizId INTEGER NOT NULL REFERENCES Quiz,
              userId INTEGER NOT NULL REFERENCES User,
              startTime INTEGER,
              endTime INTEGER,
              status VARCHAR(16)NOT NULL,
              PRIMARY KEY(quizId, userId)
              );
            `, [], InitSQLCallback(awaiter));

            db.run(`
              CREATE TABLE UserQuestion(
              questionIndex INTEGER NOT NULL,
              quizId INTEGER NOT NULL REFERENCES Quiz,
              userId INTEGER NOT NULL REFERENCES User,
              selected INTEGER NOT NULL,
              percentTimeSpent NUMBER NOT NULL,
              PRIMARY KEY(questionIndex, quizId, userId)
              );
            `, [], InitSQLCallback(awaiter));
        });
    });
}


export async function PopulateDatabase(db: Database): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        const awaiter = new MultipleEventAwaiter(
            () => resolve(),
            err => reject(err),
            3
        );

        const path = './data/quizes/';
        const files: string[] = fs.readdirSync(path);

        awaiter.NewEvents(files.length);
        awaiter.EventFinished();

        for (const file of files) {
            AddQuiz(db, JSON.parse(fs.readFileSync(path + file).toString()) as any as QuizDataDTO).then(() => {
                awaiter.EventFinished();
            });
        }

        db.run(
            `INSERT INTO User VALUES (1, 'user1', ?);`,
            [HashPassword('user1')],
            InitSQLCallback(awaiter)
        );

        db.run(
            `INSERT INTO User VALUES (2, 'user2', ?);`,
            [HashPassword('user2')],
            InitSQLCallback(awaiter)
        );
    });
}

async function runAll() {
    try {
        await CreateDatabase(databaseConn);
        await PopulateDatabase(databaseConn);
    } catch(err) {
        console.log(err);
    }
}

runAll()
    .then(() => process.exit(0))
    .catch(err => process.exit(1));

import { expect } from 'chai';
import { driver } from 'mocha-webdriver';

const standardLogin = async () => {
    await driver.find('#input-username').sendKeys('user1');
    await driver.find('#input-password').sendKeys('user1');
    await driver.find('.button-login').doClick();
}


describe('User', () => {
    describe('Login', async () => {
        beforeEach(async () => {
            await driver.manage().deleteAllCookies();
            await driver.get('http://localhost:3000');
        });

        it('should go well with good credentials', async function () {
            this.timeout(20000);
            await standardLogin();
            expect(await driver.find('.intro-message-title').getText()).to.include('Wybierz quiz:');
        });

        it('should not go well with bad password', async function () {
            this.timeout(20000);
            await driver.find('#input-username').sendKeys('user1');
            await driver.find('#input-password').sendKeys('user1notthispass');
            await driver.find('.button-login').doClick();
            expect(await driver.find('.app-message-wrapper').getText()).to.include('Invalid username or password');
        });
    });

    describe('Password change', async () => {
        beforeEach(async () => {
            await driver.manage().deleteAllCookies();
            await driver.get('http://localhost:3000');
            await standardLogin();
            await driver.find('.button-important').doClick();
        });

        const tryFilled = async (old, new1, new2) => {
            await driver.find('#pwd-change-old').sendKeys(old);
            await driver.find('#pwd-change-new').sendKeys(new1);
            await driver.find('#pwd-change-rep').sendKeys(new2);
            await driver.find('.button-changepwd').doClick();
        }

        it('should be able to change password', async function () {
            this.timeout(20000);

            // First change
            await tryFilled('user1', 'zmiana', 'zmiana');
            expect(await driver.find('.app-message-wrapper').getText()).to.equal('Pomyślna zmiana hasła.');

            // Go back
            await tryFilled('zmiana', 'user1', 'user1');
            expect(await driver.find('.app-message-wrapper').getText()).to.equal('Pomyślna zmiana hasła.');
        });

        it('should be logged out of different session when changing password', async function () {
            this.timeout(20000);
            const oldSessionId: string = (await driver.manage().getCookie('sessionId')).value;

            // We can change to same password
            await tryFilled('user1', 'user1', 'user1');
            expect(await driver.find('.app-message-wrapper').getText()).to.equal('Pomyślna zmiana hasła.');

            // Restore old session and reopen
            await driver.manage().addCookie({name: "sessionId", value: oldSessionId});
            await driver.get('http://localhost:3000');
            expect(await driver.find('.button-login').isPresent()).to.equal(true);


        });
    });
})

describe('Quiz', async () => {
    beforeEach(async () => {
        await driver.manage().deleteAllCookies();
        await driver.get('http://localhost:3000');
        await standardLogin();
    });

   it ('should be able to be solved', async function () {
        this.timeout(20000);

       await (await driver.findAll('.button-select-quiz'))[0].doClick();

       await driver.find('.start-button').doClick();

       while (true) {
           await driver.find('.answer-button').doClick();
           await driver.find('.confirm-answer-button').doClick();
           const next = await driver.find('.nav-button-next');
           if (!await next.isEnabled()) {
               break;
           }
           await next.doClick();
       }


       await driver.find('.stop-button').doClick();

       expect(await driver.find('#summary-self').getText()).to.include('Zdobyłeś');
   });

   it ('should bot be solveable again', async function() {
       this.timeout(20000);

       await (await driver.findAll('.button-select-quiz'))[0].doClick();

       expect((await driver.findAll('.start-button')).length).to.equal(0);
   })
});
